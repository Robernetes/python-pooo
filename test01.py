'''
Realizar un programa que conste de una clase llamada Alumno 
que tenga como atributos el nombre y la nota del alumno.
Definir los métodos para inicializar sus atributos, imprimirlos
y mostrar un mensaje con el resultado de la nota y si ha aprobado o no
'''

class Alumn:
    def __init__(self, name, nota):
        self.name = name
        self.nota = nota

    def mostrarNombre(self):
        print(f"Estudiante: {self.name} ")

    def mostrarNota(self):
        print(f"Su nota es {self.nota} ")

    def msjAprobado(self):
        if self.nota == "A":
            print("Aprobado")
        elif self.nota == "B":
             print("Aprobado")
        elif self.nota == "C":
             print("Aprobado Regular")
        elif self.nota == "D":
            print("Reprobado")


estuidnate1 = Alumn("Roberto", "C")
estuidnate1.mostrarNombre()
estuidnate1.mostrarNota()
estuidnate1.msjAprobado()
